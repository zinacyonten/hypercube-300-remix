use <../common/parametric_bracket.scad>

difference()
{
    rotate([90,0,0])
        t_bracket();
    translate([0,50,0])
        cube(30, center=true);
    translate([0,-50,0])
        cube(30, center=true);
	translate([-12,0,0])
		cube([4,80,28], center=true);
}


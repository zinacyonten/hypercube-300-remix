$fn=30;

translate([0,12,0])
rotate([0,0,180])
difference()
{
    rotate([0,0,180])
    translate([23.5,-16,0])
        import("AM8_Right_Side_Y_Chain_Link.stl");

    cube([30,20,20]);
}

difference()
{
    translate([-23,-88,0])
    {
        cube([23,100,2]);
        translate([0,20,0])
        cube([2,80,5]);
    }

    translate([-11.5,-78,-3])
        cylinder(d=5.6, h=10);
}

translate([-2,-68,0])
    cube([2,80,5]);

translate([-2,12,2])
rotate([0,-90,180])
linear_extrude(2, convexity=10)
    polygon([[0,0],[10,0],[0,10]]);

translate([-23,12,2])
rotate([0,-90,180])
linear_extrude(2, convexity=10)
    polygon([[0,0],[10,0],[0,10]]);


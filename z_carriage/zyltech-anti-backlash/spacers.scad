$fn=90;

// translate([-15,-15,-1])
// rotate([-90,0,0])
// color([.8,.8,.8])
// import("Hypercube_Zylech_anti_backlash_mount-v2..stl");

linear_extrude(.047*25.4, convexity=10)
difference()
{
    minkowski()
    {
        square([32,12], center=true);
        circle(d=8);
    }
    translate([13.5,0])
        circle(d=5.3);
    translate([-13.5,0])
        circle(d=5.3);
}

translate([0,-30,0])
{
    linear_extrude(.033*25.4, convexity=10)
    difference()
    {
        minkowski()
        {
            square([32,12], center=true);
            circle(d=8);
        }
        translate([13.5,0])
            circle(d=5.3);
        translate([-13.5,0])
            circle(d=5.3);
    }
}
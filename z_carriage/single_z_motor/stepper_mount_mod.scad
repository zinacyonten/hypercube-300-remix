difference()
{
    union()
    {
        difference()
        {
            import("stepper_mount.stl");
            translate([-24,0,0])
                cube([100,5,15]); // remove alignment rib
        }
        translate([-31,-5,5])
            cube([100,5,10]); // fill holes

        translate([-34.5,-5,0])
            cube([110,5,32]); // extension
        translate([-34.5,0,7])
            cube([110,20,5]); // lip
    }
    translate([60,2.5,22])
    rotate([90,0,0])
        cylinder(d=5.4, $fn=30, h=10); // drill new mounting holes
    translate([-25,2.5,22])
    rotate([90,0,0])
        cylinder(d=5.4, $fn=30, h=10);
    translate([60,10,4])
        cylinder(d=5.4, $fn=30, h=10);
    translate([-25,10,4])
        cylinder(d=5.4, $fn=30, h=10);
}

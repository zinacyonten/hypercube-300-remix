$fn=30;

translate([-3,-3,0])
difference()
{
    cylinder(d=4.4, h=2.6);
    cylinder(d=2.8, h=2.6);
}

translate([3,-3,0])
difference()
{
    cylinder(d=4.4, h=2.6);
    cylinder(d=2.8, h=2.6);
}

translate([-3,3,0])
difference()
{
    cylinder(d=4.4, h=2.6);
    cylinder(d=2.8, h=2.6);
}

translate([3,3,0])
difference()
{
    cylinder(d=4.4, h=2.6);
    cylinder(d=2.8, h=2.6);
}

$fn=45;

translate([0,0,2])
union()
{
    %import("Arduino Due.stl");

    translate([-1.0,10.8,6])
        %import("RAMPS-FD.stl");
}

// translate([-34.6,24.2,5.6])
// translate([40.33,24.2,5.6])
// translate([-35.87,-24.06,5.6])
translate([46.68,-24.06,5.6])
difference()
{
    cylinder(d=4.4, h=11);
    cylinder(d=2.8, h=11);
}
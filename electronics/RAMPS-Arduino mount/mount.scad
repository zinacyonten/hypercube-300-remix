$fn=45;

translate([0,0,2])
union()
{
    %import("Arduino Due.stl");

    translate([-1.0,10.8,6])
        %import("RAMPS-FD.stl");
}

translate([0,7,-0.5])
union()
{
    translate([0,75,20])
    rotate([90,0,0])
        %import("../originals/lcd-housing/LCD-RPi3_Enclosure_Make_1.stl");

    translate([-50,-28.5,24])
    rotate([0,90,0])
        %import("20mm-tslot/20mm-M5-W1pt6-X1-100mm.stl");
}

difference()
{
    union()
    {
        translate([5,-10,0])
        minkowski()
        {
            cube([90,80,2], center=true);
            sphere(r=1);
        }
        translate([5,-40,.5])
        minkowski()
        {
            cube([90,20,2], center=true);
            sphere(r=1);
        }
        translate([-34.6,24.2,0])
            cylinder(d=5.6, h=4);
        translate([40.33,24.2,0])
            cylinder(d=5.6, h=4);
        translate([-35.87,-24.06,0])
            cylinder(d=5.6, h=4);
        translate([46.68,-24.06,0])
            cylinder(d=5.6, h=4);
    }
    translate([-34.6,24.2,-2])
        cylinder(d=2.6, h=8);
    translate([40.33,24.2,-2])
        cylinder(d=2.6, h=8);
    translate([-35.87,-24.06,-2])
        cylinder(d=2.6, h=8);
    translate([46.68,-24.06,-2])
        cylinder(d=2.6, h=8);

    translate([40,-41.5,-2])
        cylinder(d=5.4, h=6);
    translate([-30,-41.5,-2])
        cylinder(d=5.4, h=6);

    translate([3,0,0])
    minkowski()
    {
        cube([65,40,4], center=true);
        sphere(d=3);
    }
}

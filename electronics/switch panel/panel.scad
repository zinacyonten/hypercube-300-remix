// switch panel for Hypercube
// Scott Alfter
// 3 March 2019
// CC-BY

$fn=45;
roundness=1;         // round off the edges
looseness=0.4;       // make holes oversize by this amount
label_thickness=0.3; // should be equal to first-layer height
nozzle_diam=0.4;

// print label() in one color, then print panel() in another

// label();
panel();

module label()
{
    centering_frame(130);

    labels();
}

// turn face-down and center
module panel()
{
    centering_frame(110);

    difference()
    {
        translate([-51.5,26.5,68.5])
        rotate([115,0,0])
            body();
        holes();
        labels();
    }
}

module centering_frame(size)
{
    linear_extrude(label_thickness, convexity=true)
    difference()
    {
        square(size, center=true);
        square(size-2*nozzle_diam, center=true);
    }
}

module holes()
{
    translate([0,0,2])
    {
        // E-stop switch
        translate([-25,0,0])
            cylinder(d=21.7+looseness, h=6, center=true);
            
        // reset button
        translate([10,-20,0])
            cylinder(d=6.8+looseness, h=6, center=true);
        
        // RAMPS-stack power swithch
        translate([35,-20,0])
            cylinder(d=5.8+looseness, h=6, center=true);

        // Raspberry Pi power switch
        translate([10,20,0])
            cylinder(d=5.8+looseness, h=6, center=true);
        
        // light switch
        translate([35,20,0])
            cylinder(d=5.8+looseness, h=6, center=true);
    }
}

module labels()
{
    translate([0,0,label_thickness])
    rotate([180,0,0])
    linear_extrude(label_thickness, convexity=10)
    {
        translate([-25,25,0])
            text("Emergency", size=5, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
        translate([-25,-25,0])
            text("Stop", size=5, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
        translate([10,27,0])
            text("Reset", size=4, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
        translate([35,27,0])
            text("Printer", size=4, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
        translate([10,-13,0])
            text("RPi", size=4, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
        translate([35,-13,0])
            text("Lights", size=4, halign="center", valign="center", font="Nimbus Sans L:style=Bold");

        translate([0,-40,0])
            text("Hypercube", size=10, halign="center", valign="center", font="Nimbus Sans L:style=Bold");
    }

}

module body()
{
    difference()
    {
        minkowski()
        {
            translate([51.5,-14.6625,97])
                cube([103-2*roundness,29.325-2*roundness,4-2*roundness], center=true);
            sphere(r=roundness);
        }

        translate([18.5,-10,95])
            cylinder(d=5.4, h=10, center=true);

        translate([81.5,-10,95])
            cylinder(d=5.4, h=10, center=true);
    }

    translate([51.5,-49.5,52.35])
    rotate([65,0,0])
    minkowski()
    {
        cube([103-2*roundness,102-2*roundness,3-2*roundness], center=true);
        sphere(r=roundness);
    }

    minkowski()
    {
        difference()
        {
            translate([51.5,-61,3.5])
            rotate([-25,0,0])
                cube([103-2*roundness,23-2*roundness,4-2*roundness], center=true);

            translate([52.5,-50,-5+roundness])
                cube([105,100,10], center=true);
        }
        sphere(r=roundness);
    }

    translate([4-roundness,0,0])
    rotate([90,0,-90])
    minkowski()
    {
        linear_extrude(4-2*roundness, convexity=10)
            polygon([
                [17+roundness,0+roundness],
                [56.7-roundness,0+roundness],
                [72-roundness,7.1],
                [29.35-roundness,98.5-roundness],
                [0+roundness,98.5-roundness],
                [0+roundness,17]
            ]);
        sphere(r=roundness);
    }

    translate([103.00-roundness,0,0])
    rotate([90,0,-90])
    minkowski()
    {
        linear_extrude(4-2*roundness, convexity=10)
            polygon([
                [17+roundness,0+roundness],
                [56.7-roundness,0+roundness],
                [72-roundness,7.1],
                [29.35-roundness,98.5-roundness],
                [0+roundness,98.5-roundness],
                [0+roundness,17]
            ]);
        sphere(r=roundness);
    }
}

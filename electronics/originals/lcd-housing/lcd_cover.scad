translate([0,-39,-92.4])
rotate([-65,0,0])
    %import("LCD-RPi3_Enclosure_Make_1.stl");

translate([0.25,0,0])
linear_extrude(2.5, convexity=10)
minkowski()
{
    square([68.5,41], center=true);
    circle(d=4, $fn=15);
}

rotate_extrude($fn=30)
    polygon([[0,0],[0,12],[4,12],[2,0]]);
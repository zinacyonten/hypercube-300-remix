include <knurledFinishLib.scad>

offsetMargin = 0.1;
$fn = 64;

encoderShaftDiameter = 6.0;
encoderShaftFlatLength = 7.0;
encoderShaftFlatOffset = 1.5;

knobDiameter = 37.0;
knobHeight = encoderShaftFlatLength + 1.0;
knurlWidth = 3;      // Knurl polyhedron width
knurlHeight = 4;      // Knurl polyhedron height
knurlDepth = 1.5;    // Knurl polyhedron depth
endSmoothing = 1;      // Cylinder ends smoothed height
surfaceSmoothing = 50;      // [ 0% - 100% ] Knurled surface smoothing amount

fingerHoleDiameter = 13.0;
fingerHoleDepth = 4.0;
fingerHoleOffsetFromCenterOfKnob = knobDiameter / 2 - fingerHoleDiameter / 2 - 2;
// Don't change this unless you know what you're doing
fingerHoleStretchedHeightPercentage = fingerHoleDepth / (fingerHoleDiameter / 2);

createKnob();

module createKnob()
{
    union()
    {
        difference()
        {
            // Main Knob Body
            knurled_cyl(knobHeight, knobDiameter, knurlWidth, knurlHeight, knurlDepth, endSmoothing, surfaceSmoothing);
            
            // Shaft Cutout
            translate(v = [0, 0, -offsetMargin])
            {
                cylinder(r = encoderShaftDiameter / 2 + offsetMargin, h = encoderShaftFlatLength);
            }
            
            // Finger Hole
            translate(v = [fingerHoleOffsetFromCenterOfKnob, 0, fingerHoleDiameter / 2 + knobHeight - fingerHoleDepth - fingerHoleDiameter / 2 * (1.0 - fingerHoleStretchedHeightPercentage)])
            {
                scale(v = [1.0, 1.0, fingerHoleStretchedHeightPercentage])
                {
                    sphere(r = fingerHoleDiameter / 2);
                }
            }
        }
    }
    
    // Shaft Flat
    translate(v = [-offsetMargin - encoderShaftDiameter / 2, encoderShaftDiameter / 2 - encoderShaftFlatOffset + offsetMargin, 0])
    {
        cube(size = [encoderShaftDiameter + 2 * offsetMargin, encoderShaftFlatOffset + offsetMargin, encoderShaftFlatLength + offsetMargin])
        {
            
        }
    }
}
$fn=90;

id=12.7; // original: 12.4
notch_width=2.6; // original: 3.2
notch_depth=1.6; // original: 1.6
wall_thickness=4.6; // original: 4

    intersection()
    {
        difference()
        {
            union()
            {
                translate([18.55,-27.55,0.5])
                minkowski()
                {
                    cylinder(d=id+wall_thickness-1, h=35.1);
                    sphere(d=1);
                }
            }

            translate([18.55,-27.55,0])
                cylinder(d=id, h=36.1);

            translate([18.55,-27.55,18.05]) // locating rings
            rotate_extrude() // change to self-supporting trapezoidal design
                polygon([[id/2,(notch_width+notch_depth)/2],[id/2+notch_depth,notch_width/2],[id/2+notch_depth,-notch_width/2],[id/2,-(notch_width+notch_depth)/2]]);
        }

        translate([18.55,-27.55,(35.1+notch_width)/2])
            cylinder(d=id+wall_thickness, h=25, center=true);
    }


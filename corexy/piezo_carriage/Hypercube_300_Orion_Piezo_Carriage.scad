$fn=90;

igus=1; // create housings for Igus JUM-01-10 liners instead of integrated bushings

// settings for use with Igus liners
id=12.7; 
notch_width=2.6; 
notch_depth=1.6; 
wall_thickness=4.6; 

translate([32,-100,25])
rotate([0,90,90])
{
    translate([25,32,100])
    rotate([90,0,-90])
    difference()
    {
        // option 1: integrated bushing
        if (!igus)
            import("Hypercube_300_Piezo_Carriage.repaired.stl");
        else
        {
            // option 2: housings for Igus JUM-01-10 liners
            difference()
            {
                union()
                {
                    import("Hypercube_300_Piezo_Carriage.repaired.stl");

                    translate([18.55,-27.55,0.5])
                    minkowski()
                    {
                        cylinder(d=id+wall_thickness-1, h=35.1);
                        sphere(d=1);
                    }

                    translate([18.55,-79.05,0.5])
                    minkowski()
                    {
                        cylinder(d=id+wall_thickness-1, h=35.1);
                        sphere(d=1);
                    }
                }

                translate([18.55,-27.55,0])
                    cylinder(d=id, h=36.1);

                translate([18.55,-79.05,0])
                    cylinder(d=id, h=36.1);

                translate([18.55,-27.55,18.05]) // locating rings
                rotate_extrude()
                    polygon([[id/2,(notch_width+notch_depth)/2],[id/2+notch_depth,notch_width/2],[id/2+notch_depth,-notch_width/2],[id/2,-(notch_width+notch_depth)/2]]);

                translate([18.55,-79.05,18.05])
                rotate_extrude()
                    polygon([[id/2,(notch_width+notch_depth)/2],[id/2+notch_depth,notch_width/2],[id/2+notch_depth,-notch_width/2],[id/2,-(notch_width+notch_depth)/2]]);
            }
        }

        translate([51.6,-33,18]) // remove Piezo20 mount
            cube([40,10,40], center=true);

        translate([30,-61.5,46]) // remove electronics mount
            cube([3.5,32,20], center=true);
        translate([31,-45.5,35.25]) 
        rotate([90,0,0])
        linear_extrude(32, convexity=10)
        difference()
        {
            square([2,2]);
            circle(d=1.5);
        }
    }

    intersection()
    {
        translate([7,-17.5,69])
        rotate([0,180,0])
        color([1,.5,.5])
        difference()
        {
            union()
            {
                import("Orion_Screw_Top.stl");
                cylinder($fn=45, d=10, h=4); // plug center hole
                //cylinder($fn=45, d=7, h=8); // add nipple for Teflon tube into hotend
            }
            cylinder($fn=45, d=9.5, h=8); // drill it out for M4 pneumatic fitting
        }

        translate([7,-15.5,65.5]) // shave off excess
        difference()
        {
            minkowski()
            {
                cube([34,38,5], center=true);
                sphere(d=2);
            }

            translate([13.5,21,-5.5])
            rotate([90,30,0])
                cylinder(d=7, $fn=6, h=8);
        }
    }

    translate([7,-15.5,65.5]) // fill in the gaps
    {
        difference()
        {
            minkowski()
            {
                cube([34,38,5], center=true);
                sphere(d=2);
            }

            translate([0,-2,0])
            cube([33,33,7], center=true);

            translate([13.5,21,-5.5])
            rotate([90,30,0])
                cylinder(d=7, $fn=6, h=8);
        }
        translate([0,14,0])
            cube([17,1.5,7], center=true);
    }
}

translate([49.5,-27,18])
rotate([90,0,0])
difference()
{
    cylinder(d=15, h=4);
    translate([0,0,-.5])
    cylinder(d=9.5, h=5);
}
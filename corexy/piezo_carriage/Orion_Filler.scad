$fn=30;
difference()
{
    cube([35,35,1.6], center=true);
    translate([-13.25,-13,-.9])
        cylinder(d=3.5, h=1.8);
    translate([13.25,-13,-.9])
        cylinder(d=3.5, h=1.8);
    translate([-13.25,13,-.9])
        cylinder(d=3.5, h=1.8);
    translate([13.25,13,-.9])
        cylinder(d=3.5, h=1.8);
    translate([0,0,-.9])
        cylinder(d=6, h=1.8);
}
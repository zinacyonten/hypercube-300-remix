$fn=20; // increase to 90 for production

// development use
fit_check();
// joiner_section();
// joiner_clamp();

// pieces to print (one each; render one at a time):
// carriage_section();
// joiner_clamp_a();
// joiner_clamp_b();
// joiner_top();
frame_section();

// rotate([0,-90,0])
//     carriage_section();

// joiner_clamp_a();

// rotate([180,0,0])
//     joiner_clamp_b();

// joiner_top();

// frame_section();

// fit check
module fit_check()
{
    hc300_mockup();
    translate([113,9,43.25])
        carriage_section();
    translate([1,0,25.8])
        joiner_section();
    translate([-38.5,180,-42])
        frame_section();
}

// mockups: XY joiner, carriage, rods, and part of the frame
module hc300_mockup()
{
    translate([-27,0,0])
    rotate([0,90,0])
        %import("../spiral bearing XY joiner/spiral bushing XY joiner.stl");

    translate([60,0,25.8])
    rotate([0,90,0])
        %difference()
        {
            cylinder(d=10, h=200, center=true);
            cylinder(d=8, h=201, center=true);
        }
    translate([60,0,-25.8])
    rotate([0,90,0])
        %difference()
        {
            cylinder(d=10, h=200, center=true);
            cylinder(d=8, h=201, center=true);
        }

    translate([115,-18.5,53.5])
    rotate([90,0,90])
        %import("../piezo_carriage/Hypercube_300_Orion_Piezo_Carriage_Igus.stl");

    translate([-27,70,0])
    rotate([90,0,0])
        %cylinder(d=8, h=200, center=true);

    translate([-2.25,170,0])
    rotate([0,90,-90])
        %import("../original/HC_Y_Shaft_Clamp_8mm_Make_3.stl"); 

    translate([-27,180,-100])
        %cube([20,20,300], center=true);

    translate([-27,180,46.9])
    rotate([-90,0,-90])
        %import("../../frame_braces/hc300_top_corner.stl");

    translate([-27,70,46.9])
        %cube([20,200,20], center=true);

    translate([-10,144,-17])
    rotate([180,0,-90])
        %import("../original/HC300_XY_Motor_right_Make_1.stl");

    translate([-27,180,-91])
    rotate([0,0,90])
    mirror([1,0,0])
        %import("../../frame_braces/hc300_front_left_t.stl");

    translate([-4,142,-63])
        %import("../../z_carriage/single_z_motor/tmp/Nema 17HS4401-S.stl");
}

// joiner section
module joiner_section()
{
    joiner_clamp();
    joiner_top();

    // translate([0,0,-51.6])
    // rotate([180,0,0])
    //     joiner_clamp();
    // joiner_bottom();
}

// top joiner
module joiner_top()
{
    // connection point
    translate([4,-6.5,42])
    rotate([180,0,180])
        chain_m();

    // arm assembly
    difference()
    {
        union()
        {
            // arm
            translate([0,-.5,21.75])
                cube([8,11,14.5], center=true);
            translate([0,-12,27])
                cube([8,12,4], center=true);

            // foot
            difference()
            {
                translate([0,0,16.5])
                    cube([19,12,4], center=true);
                translate([6,0,16.5])
                    cylinder(d=3.4, h=4, center=true);
                translate([-6,0,16.5])
                    cylinder(d=3.4, h=4, center=true);
            }
        }
        translate([6,0,20.5])
            cylinder(d=6.4, h=6, center=true);
        translate([-6,0,19.5])
            cylinder(d=6.4, h=6, center=true);
    }
}

// bottom joiner
module joiner_bottom()
{
    // connection point
    translate([0,-4,-66])
    rotate([0,180,90])
        chain_f();

    // mounting plate
    translate([0,0,-67.5])
    difference()
    {
        cube([19,10,3], center=true);
        translate([6,0,0])
            cylinder(d=3.4, h=3, center=true);
        translate([-6,0,0])
            cylinder(d=3.4, h=3, center=true);
        translate([6,0,-1])
            cylinder(d=6.4, h=1, center=true);
        translate([-6,0,-1])
            cylinder(d=6.4, h=1, center=true);
    }
}

// joiner clamp

module joiner_clamp_a()
{
    intersection()
    {
        joiner_clamp();
        translate([0,0,15])
            cube([200,200,30], center=true);
    }
}

module joiner_clamp_b()
{
    intersection()
    {
        joiner_clamp();
        translate([0,0,-15])
            cube(30, center=true);
    }
}

module joiner_clamp()
{
    rotate([0,90,0])
    difference()
    {
        minkowski()
        {
            union()
            {
                difference()
                {
                    cylinder(d=27, h=19, center=true);
                    translate([11,0,0])
                        cube([7,27,19], center=true);
                }
                translate([-7,0,0])
                    cube([14,27,19], center=true);
            }
            sphere(d=1, center=true);
        }
        cylinder(d=10, h=20, center=true);

        // M3 nut inserts
        translate([7,10.2,5])
        rotate([0,90,0])
            cylinder(d=6.4, h=10, $fn=6, center=true);
        translate([7,-10.2,5])
        rotate([0,90,0])
            cylinder(d=6.4, h=10, $fn=6, center=true);
        translate([7,10.2,-5])
        rotate([0,90,0])
            cylinder(d=6.4, h=10, $fn=6, center=true);
        translate([7,-10.2,-5])
        rotate([0,90,0])
            cylinder(d=6.4, h=10, $fn=6, center=true);

        // holes to clear M3 screws
        translate([0,-10.2,-5])
        rotate([0,90,0])
            cylinder(d=3.4, h=30, center=true);
        translate([0,10.2,-5])
        rotate([0,90,0])
            cylinder(d=3.4, h=30, center=true);
        translate([0,-10.2,5])
        rotate([0,90,0])
            cylinder(d=3.4, h=30, center=true);
        translate([0,10.2,5])
        rotate([0,90,0])
            cylinder(d=3.4, h=30, center=true);

        // countersink screw holes
        translate([-12,10.2,5])
        rotate([0,90,0])
            cylinder(d=6.4, h=20, center=true);
        translate([-12,10.2,-5])
        rotate([0,90,0])
            cylinder(d=6.4, h=20, center=true);
        translate([-12,-10.2,5])
        rotate([0,90,0])
            cylinder(d=6.4, h=20, center=true);
        translate([-12,-10.2,-5])
        rotate([0,90,0])
            cylinder(d=6.4, h=20, center=true);

        // holes for attachments
        translate([-10,0,6])
        rotate([0,90,0])
            cylinder(d=3.4, h=20, center=true);
        translate([-10,0,-6])
        rotate([0,90,0])
            cylinder(d=3.4, h=20, center=true);
    }

    joiner_clamp_ext();
}

module joiner_clamp_ext()
{
    difference()
    {
        union()
        {
            translate([-24,-11.5,6.5])
                cube([34,5,6], center=true);
            translate([-19.5,-9,6.5])
            rotate([90,0,0])
                cylinder(d=5.4, h=1.9, center=true);
        }

        translate([-19.5,0,6.5])
        rotate([90,0,0])
            cylinder(d=3.4, h=30, center=true);
    }

    translate([-50,-5,13])
    rotate([0,180,90])
        chain_f();

    translate([-42,-14,3.5])
    rotate([0,90,0])
    linear_extrude(33, convexity=10)
        polygon([[0,0],[0,5],[3.5,0]]);
}

// carriage section
module carriage_section()
{
    difference()
    {
        minkowski()
        {
            union()
            {
                cube([3, 7, 12.5], center=true);
                translate([0,0,6.25])
                rotate([0,90,0])
                    cylinder(d=7, h=3, center=true);
                translate([0,0,-6.25])
                rotate([0,90,0])
                    cylinder(d=7, h=3, center=true);
            }
            sphere(d=1, center=true);
        }
        translate([0,0,6.25])
        rotate([0,90,0])
            cylinder(d=3.4, h=5, center=true);
        translate([0,0,-6.25])
        rotate([0,90,0])
            cylinder(d=3.4, h=5, center=true);
    }
    translate([-2, -15.5, -7.25])
        chain_f();

    translate([0,-4,-.7])
    cube([4,2,13.4], center=true);
    // translate([1.5,-3.5,-1.5])
    // minkowski()
    // {
    //     linear_extrude(3, convexity=10)
    //         polygon([[0,0],[7,0],[0,7]]);
    //     sphere(d=1, center=true);
    // }
}

// split a chain link into male & female halves for integration with the mounts

module chain_f()
{
    // intersection()
    // {
    //     translate([-10,13,0])
    //         import("Anet_A8_Chain.stl");
    //     translate([0,-15,0])
    //         cube([20,30,15]);
    // }

    intersection()
    {
        translate([-3,-9,0])
            import("CableChain_BaseUnit.stl");
        translate([0,-15,0])
            cube([20,30,15]);
    }
    translate([0,-9,11])
        cube([6,20,2]);
}

module chain_m()
{
    // difference()
    // {
    //     union()
    //     {
    //         intersection()
    //         {
    //             translate([-10,13,0])
    //                 import("Anet_A8_Chain.stl");
    //             translate([-11,-15,0])
    //                 cube([20,30,15]);
    //         }
    //         translate([9,0,1.2])
    //             cube([6,22.5,2.4], center=true);
    //         translate([9,-10,6.7])
    //             cube([6,2.5,13.4], center=true);
    //         translate([9,10,6.7])
    //             cube([6,2.5,13.4], center=true);
    //     }
    //     translate([5.5,-8.75,2.4])
    //         cube([6.5,17.5,11]);
    // }

    intersection()
    {
        translate([-1,-11.2,0])
            import("CableChain_BaseUnit.stl");
        translate([-11,-15,0])
            cube([20,30,15]);
    }
    translate([2,-11.2,11])
        cube([6,20,2]);

}

// frame section

module frame_section()
{
    difference()
    {
        cube([3,20,20], center=true);
        translate([-1.75,0,0])
        rotate([0,90,0])
            cylinder(d=5.4, h=3.5);
    }

    translate([-12.5,-18,-10])
    rotate([0,0,90])
        chain_m();

    translate([-10.75,0,-8.5])
        cube([21.5,20,3], center=true);
            
    translate([-20,0,-2.5])
    difference()
    {
        cube([3,20,12], center=true);
        translate([0,0,2.5])
        rotate([0,90,0])
            cylinder(d=10, h=5, center=true);
    }

    translate([-10,8.5,-2.5])
        cube([22,3,12], center=true);

    translate([-2.3,-9,-2.5])
        cube([3,3,12], center=true);
}

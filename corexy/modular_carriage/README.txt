                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2517628
Hypercube 300 CoreXY by adrianm1972 is licensed under the Creative Commons - Attribution - Non-Commercial - Share Alike license.
http://creativecommons.org/licenses/by-nc-sa/3.0/

# Summary

The Hypercube 300 is an extensive remix of Tech2C's Hypercube CoreXY 3D Printer.  My goal with this project was to make a low cost, high build volume 3D printer with print quality rivaling the best, most expensive consumer printers.  I feel I succeeded.  The printer can be built with top quality components for less than $600 and with budget conscious components for less the $500.  The Hyper cube 300 features a true 300mm^3 build volume.  It's a few mm more than 300mm in every axis so you can successfully print right to the limits. The print quality from the first print was outstanding and has gotten even better with basic tuning.

Please see the thing files for an up to date BOM with  descriptions and quantities of what you need.

I will update this shortly with how many to make of my STL's.

You will need to go to Tech2C's Hypercube Thingiverse page, download the print his X and Y motor mounts, belt idler bearing mounts, the XY carriage and all the hot end, fan and probe parts.  I will update this post shortly with links to the files and quantities for those.

I am a regular on the Hypercube CoreXY 3D Printer Group ( https://www.facebook.com/groups/1871049783171141/ ).  Lots of great folks there.  If you have questions ask here or join the group and ask there.

9/14/2017 - Updated a few parts while hurricane Irma was wreaking havoc around me.

- Switched from 10mm back to 8mm Y rods.  They are plenty strong and a LM8LUU bearings is literally 1/2 the weight of an LM10LUU bearing.

- Updated the XY joiner and its bearing cap to make it stronger, lighter and look nicer.  I tweaked how the bearing cap clamps to the joiner to allow a better grip on bearings for those using IGUS or printed solid bushings.

- Updated the Y rods mounts to hold 8mm rods.

9/22/2017 - Added optional parts for 10mm Y Rods after the BOM file in the things list.

10/8/2017 - Revised XYJoiner and its bearing cap.  Also added a integrated bearing cap/bushing for those wanting to use a printed bushing instead of a LM8LUU bearing

10/30/2017 - Revised all "Thing" names as follows

1. All STL files show the quantity necessary at the end of the file name
2. All STL files indicate comparability of the file
   a. STL's starting with HC fit the original Tech2C Hypercube and my Hypercube 300
   b. STL's starting with HC300 fit the Hypercube 300 only
   c. STL's with no prefix can fit any printer that uses 2020 type extrusions

1/17/2018 - Updated all STL files to the latest and greatest.

1. New 12864 Enclosure now has cutouts and when combined with the RPi3 adapter place and spacers will allow for an RPi3 to be tucked in behind the LCD.
2. The The XY Motor mounts are now HC300 only parts. The cutouts for a y shaft mounted limit switch have been removed.  They are stronger and nicer looking than the standard HC mounts.
3. The RAMPS box is more simple and prints 10X faster!
4.  New z bearing carriages are completely different and optimized  for a 4 z rod build.
5.  The bushings for the X carbon tubes no longer take IGUS bushing.  Now they are one piece bushings that ride directly on the rod.  Note you may have to print a few of these with different extrusion multipliers to get the right fit.

Update 4/2/2018

 - Made lower Z rod mounts stronger
 - Made Z bearing carriages prettier, easier to assemble and faster to print.


# Print Settings

Printer Brand: Prusa
Printer: Prusa Mk2
Resolution: 0.2mm
Infill: 35%

Notes: 
3 shells top, bottom, perimeter.

It's pretty straight forward printing the necessary parts if you printer is working decent.
$fn=90;

//    %import("HC_X_Carrier_Bushing_-No_IGUS_Make_2.stl");

translate([25.21,0,0])
difference()
{
    union()
    {
        cylinder(d=16, h=40);

        difference()
        {
            translate([0,-1,0])
            minkowski()
            {
                rotate([90,0,0])
                linear_extrude(2.5, convexity=10)
                polygon([
                    [0,2],
                    [14,2],
                    [14,7],
                    [3,20],
                    [14,33],
                    [14,38],
                    [0,38],
                    [-14,22],
                    [-14,18],
                    [0,2]
                ]);

                sphere(2);
            }

            translate([11.25,5,4.75])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);

            translate([11.25,5,35.25])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);

            translate([-11.25,5,20])
            rotate([90,0,0])
                cylinder(d=3.5, h=12);


            translate([11.25,-2.5,4.75])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);

            translate([11.25,-2.5,35.25])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);

            translate([-11.25,-2.5,20])
            rotate([90,30,0])
                cylinder(d=6.8, h=3, $fn=6);
        }
    }

    cylinder(d=13, h=40);

    translate([-5.5,9,1.5])
    rotate([90,0,0])
        cylinder(d=1, h=18);

    translate([-5.5,9,38.5])
    rotate([90,0,0])
        cylinder(d=1, h=18);
}



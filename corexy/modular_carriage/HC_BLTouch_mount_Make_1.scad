difference()
{
    import("HC_Prox_sensor_mount_Make_1.stl");
    translate([-10,-10,-5])
        cube([20,22,25]);
}

translate([-8,-18,17.9])
difference()
{
    union()
    {
        translate([5,0,0])
            cylinder(d=10, h=3, $fn=30);
        cube([10,30,3]);
    }
    translate([5,-.85,-6])
        cylinder(d=3, h=10, $fn=30);
    translate([5,17,-6])
        cylinder(d=3, h=10, $fn=30);
}

translate([-8,9,0])
    cube([10,3,18]);

translate([2,9,18])
rotate([-90,90,90])
linear_extrude(10, convexity=10)
    polygon([[0,0],[0,5],[5,0]]);


translate([25,0,0]) // probe offset from nozzle
translate([-28,-10,9.5+8.3])
rotate([0,0,-90])
    %import("BLTouch_Model.stl");

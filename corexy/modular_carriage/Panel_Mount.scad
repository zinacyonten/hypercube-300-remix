difference()
{
    cube([42,2,18]);
    translate([0,0,15.5])
        cube([5,2,2.5]);

    translate([38.25,3,13.75])
    rotate([90,0,0])
        cylinder(d=3.4, h=4, $fn=30);

    translate([10.25,3,13.75])
    rotate([90,0,0])
        cylinder(d=3.4, h=4, $fn=30);

    translate([35.25,3,5])
    rotate([90,0,0])
        cylinder(d=3.4, h=4, $fn=30);

    translate([5.25,3,5])
    rotate([90,0,0])
        cylinder(d=3.4, h=4, $fn=30);
}


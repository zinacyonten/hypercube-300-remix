// align horizontal steel rod holders and motor mounts under top of frame

h=16.9; // gap based on 20 mm height of a metal angle brace, less thickness of printed brackets

cube([20,3,h]);
cube([3,20,h]);

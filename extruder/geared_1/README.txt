                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2596124
Geared Bowden Extruder 20 20 Mount by JonnyBrylcreem is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

This is a remix of my other extruder  https://www.thingiverse.com/thing:1758044
I've tweaked the design slightly and made a mount to fit on a 20/20 extrusion using M4 bolts. 
All hardware is as per a standard Wade's Geared Extruder and detailed on my other listing. http://reprap.org/wiki/Wade%27s_Geared_Extruder#Non-Printed_Parts

UPDATE: I've now included a stl called "Support 20 20 Inserts". It is designed to take M3x4x4 brass inserts such as http://www.ebay.co.uk/itm/M3-4-4mm-Knurled-Nuts-Thumb-Nuts-Insert-Embedded-Nuts-Brass-Solid-/122521132059?var=422958194833&hash=item1c86d41c1b:m:m5b193TN0FY7KhARXg5rmqA
These make fixing the idler block to the motor mount / support easier. The holes are intentionally too small. I melted the inserts into the plastic by heating them with a soldering iron.

# Print Settings

Resolution: 0.2mm
Infill: 100%

Notes: 
I would print the gears at 100% infill as I have had the small gear break apart after extended use. The other parts can be printed at lower infill amounts.
The small amount of support that is needed is built into the model.

# Post-Printing

The front fixings (nearest the motor) that join the idler block to the motor mount are easier to fix together if you use 10mm bolts. The rear fixings will only take 8mm bolts. It is advisable to pull the nuts down well into the recesses first before trying to screw the pieces together.
If you have problems with the recessed nuts try melting them into the plastic by heating them with a soldering iron while pressing down into the plastic.
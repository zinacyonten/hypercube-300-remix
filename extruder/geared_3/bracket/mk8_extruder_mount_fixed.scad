$fn=45;

difference()
{
    import("mk8_extruder_mount.STL");

    translate([35,17,-1])
        cylinder(d=3.4, h=10);

    translate([35+31,17,-1])
        cylinder(d=3.4, h=10);

    translate([35,17+31,-1])
        cylinder(d=3.4, h=10);

    translate([35+31,17+31,-1])
        cylinder(d=3.4, h=10);

    translate([19,5,15])
    rotate([0,90,0])
        cylinder(d=5.4, h=10);

    translate([19,60,15])
    rotate([0,90,0])
        cylinder(d=5.4, h=10);

    translate([50.5,32.5,0])
    rotate_extrude($fn=90)
        polygon([[0,0],[11.5,0],[11.5,2.5],[9.5,4.5],[0,4.5]]);
}
    

Extruder Assembly Notes
=======================

Print 1 each of the following:

* bracket/mk8_extruder_mount_fixed.stl
* extruder/BowdenDriveExtruder_Hub_v2.6.stl
* extruder/BowdenDriveExtruder_Hub_v2.62.stl
* extruder/BowdenDriveExtruder_Hub_v2.63.stl
* extruder/BowdenDriveExtruder_Hub_v2.64.stl
* extruder/BowdenDriveExtruder_Hub_v2.65.stl
* gearbox/Carrier.stl
* gearbox/Satellites.stl
* gearbox/Sun_gear.stl
* gearbox/Bearing_cap_fixed.stl
* gearbox/Crenellated_ring_V1_fixed.stl
* spacer/spacer_2mm.stl

(Note that some of these parts have "_fixed" in the name; I enlarged the
screw holes to clear the assembly and mounting hardware.  The original
versions don't have "_fixed" in their names; you'd probably have to drill
out the holes to get things together.  Also, the view_parts directory only
has the added hardware (and versions of printed parts) needed to create
the exploded view of the extruder assembly in FreeCAD.)

I used eSun PLA+, 2-3 perimeters (2, with "alternate extra perimeter" in
Cura), 4 top and bottom layers, and 100% infill.

Drill a 5mm hole through the center of the carrier front and three 3mm holes
around the carrier back.

Put an M5x25 bolt through the carrier half with the hex-head recess.  On the
protruding bolt (which will be the driveshaft), secure a 625ZZ bearing with
an M5 nut and threadlock.

Mount the satellites in the carrier with M3x16 screws, nuts, and threadlock.

Add an M3x6 screw and nut to the sun gear for retention (might need to drill
out the holes a little bit).  Place the spacer, mounting bracket, and sun
gear over the motor shaft.  Don't slide the sun gear on all the way; its
location will be determined later.

Add a 684ZZ bearing to the extruder arm (the 2.6 and 2.62 parts).  Add a 4mm
o/d threaded pneumatic fitting to the thick part of the extruder body (the
2.64 part).  Pass two M3x60 screws through the extruder body (the 2.63 and
2.64 parts) and the gearbox bearing cap.  Pass a third M3x60 screw through
the extruder arm.  Pass an M3x50 screw through the hole in the bearing cap
that's not covered by the extruder.  Put an M3x16 screw and nut on the
extruder tensioner (the 2.65 part) and place a 7.5-mm extruder spring over
the screw,

(As an aside, when I loaded the extruder parts into Paraview, selecting all
of the parts to view at the same time will show them lined up as they're
supposed to be assembled.)

Place the carrier-and-satellite assembly inside the gearbox housing (the
"crenellated ring" part), with the axle coming out the end that doesn't have
gear teeth molded in.  Place the screws hanging off the extruder body and
bearing cap through the gearbox housing, then align this assembly over the
sun gear and squeeze the stack of parts together to move the sun gear into
place on the motor shaft.  Remove the extruder-and-gearbox assembly, tighten
the screw on the sun gear to lock it in place, and put the extruder and
gearbox back on.  Tighten the screws.

Line up a Mk8 extruder drive gear on the shaft so that the groove lines up
with the filament guide hole; tighten the setscrew.

Put the extruder tensioner assembly in its slot in the extruder body. 
Position the extruder arm so it captures the end of the spring, then move
the hinge point (with M3x60 screw) over the remaining hole through the
extruder body.  Press it all the way through to the motor and tighten.


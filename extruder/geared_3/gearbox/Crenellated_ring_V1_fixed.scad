$fn=45;

difference()
{
    import("Crenellated_ring_V1.stl");

    translate([-15.5,-15.5,-1])
        cylinder(d=3.4, h=30);

    translate([15.5,-15.5,-1])
        cylinder(d=3.4, h=30);

    translate([-15.5,15.5,-1])
        cylinder(d=3.4, h=30);

    translate([15.5,15.5,-1])
        cylinder(d=3.4, h=30);
}

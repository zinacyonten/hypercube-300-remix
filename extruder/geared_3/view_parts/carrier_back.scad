difference()
{
    translate([21,16.25,0])
    difference()
    {
        import("../gearbox/Carrier.stl");

        translate([18,-15,5])
            cube([35,35,10], center=true);
    }

    translate([-12.3,0,0])
        cylinder(d=3.4, h=10, $fn=45);
    translate([5.75,10.4,0])
        cylinder(d=3.4, h=10, $fn=45);
    translate([5.75,-10.4,0])
        cylinder(d=3.4, h=10, $fn=45);
}
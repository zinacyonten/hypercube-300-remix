translate([-5.5,15,0])
difference()
{
    import("../gearbox/Satellites.stl");

    linear_extrude(10, convexity=true)
    translate([-15,0])
        square([20,50],center=true);
}
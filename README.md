Hypercube 300 Remix
===================

*WORK IN PROGRESS*

These are the design files I've either downloaded or created for my
Hypercube 300 build.  I was also tracking parts usage in the BOM for a
bit...should probably update it before it gets too far out of whack.

Not all files are in use.  For instance, I looked into a few options for
extruders, but so far I've only printed the stuff in the geared_3
subdirectory (this makes a fairly compact extruder with a 4:1 reduction
planetary gearbox).

Not everything you'll need to build the printer is in here yet!  As of this
writing (4 January 2019), I'm still building my printer.  The idea behind
this repository is to build up a record of what works and what doesn't, so
that my working configuration (whatever it ends up being) can be replicated.
